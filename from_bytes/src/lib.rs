pub use derive_from_bytes::FromBytes;
pub use zerocopy;

use byteorder::{BigEndian, ByteOrder, LittleEndian};

pub enum ByteEndianes {
    Litte,
    Big,
}

pub fn parse_f32(
    slice: &[u8],
    order: [u8; 4],
) -> Result<f32, Box<dyn std::error::Error + Send + Sync>> {
    if slice.len() >= 4 {
        Ok(BigEndian::read_f32(&[
            slice[order[0] as usize],
            slice[order[1] as usize],
            slice[order[2] as usize],
            slice[order[3] as usize],
        ]))
    } else {
        Err(format!("slice to short f32 slice: {:?}", slice).into())
    }
}

pub fn parse_bool(
    slice: &[u8],
    mask: Option<u16>,
    shift: Option<u16>,
    endianes: ByteEndianes,
) -> Result<bool, Box<dyn std::error::Error + Send + Sync>> {
    let value_u16 = parse_u16(slice, mask, shift, endianes)?;
    if value_u16 > 0 {
        Ok(true)
    } else {
        Ok(false)
    }
}

pub fn parse_u16(
    slice: &[u8],
    mask: Option<u16>,
    shift: Option<u16>,
    endianes: ByteEndianes,
) -> Result<u16, Box<dyn std::error::Error + Send + Sync>> {
    if slice.len() >= 2 {
        let mut value = match endianes {
            ByteEndianes::Big => BigEndian::read_u16(slice),
            ByteEndianes::Litte => LittleEndian::read_u16(slice),
        };
        if let Some(shift) = shift {
            value <<= shift;
        }

        if let Some(mask) = mask {
            value &= mask;
        }

        Ok(value)
    } else {
        Err("slice to short u16".into())
    }
}

pub fn parse_i16(
    slice: &[u8],
    mask: Option<u16>,
    shift: Option<u16>,
    endianes: ByteEndianes,
) -> Result<i16, Box<dyn std::error::Error + Send + Sync>> {
    if slice.len() >= 2 {
        match endianes {
            ByteEndianes::Big => Ok(BigEndian::read_i16(slice)),
            ByteEndianes::Litte => Ok(LittleEndian::read_i16(slice)),
        }
    } else {
        Err("slice to short i16".into())
    }
}

pub fn parse_u32(
    slice: &[u8],
    mask: Option<u16>,
    shift: Option<u16>,
    order: [u8; 4],
) -> Result<u32, Box<dyn std::error::Error + Send + Sync>> {
    if slice.len() >= 4 {
        Ok(BigEndian::read_u32(&[
            slice[order[0] as usize],
            slice[order[1] as usize],
            slice[order[2] as usize],
            slice[order[3] as usize],
        ]))
    } else {
        Err(format!("slice to short u32 slice: {:?}", slice).into())
    }
}
