use proc_macro::{self, TokenStream};
use proc_macro2::{Ident, Span};
use quote::{quote, ToTokens};
use std::collections::HashMap;
use syn::{parse_macro_input, punctuated::Punctuated, DeriveInput, Lit, Path, Token};
use syn_unnamed_struct::{Meta, MetaValue, NestedMeta};

const TRY_INTO_METHOD: &str = "core::convert::TryInto::try_into";
const SKIP_METHOD: &str = "core::default::Default::default()";

#[derive(Debug, Clone)]
enum FieldKind {
    FixedArray(Box<FieldKind>, usize),
    Bool,
    Float32,
    SignedInteger16,
    UnsignedInteger16,
    UnsignedInteger32,
    Into,
}

impl Default for FieldKind {
    fn default() -> Self {
        Self::UnsignedInteger16
    }
}

impl FieldKind {
    pub fn method(&self, args: &FieldArgs) -> proc_macro2::TokenStream {
        if args.register.is_some() && args.byte.is_some() {
            panic!("field can not have both byte and register");
        }

        let byte = if let Some(register) = &args.register {
            register * 2
        } else {
            args.byte.expect("missing byte or register option")
        };
        let shift: proc_macro2::TokenStream = format!("{:?}", args.shift).parse().unwrap();
        let mask: proc_macro2::TokenStream = format!("{:?}", args.mask).parse().unwrap();
        let endianes: proc_macro2::TokenStream = if let Some(endianes) = &args.endianes {
            match endianes.as_str() {
                "big" => "from_bytes::ByteEndianes::Big",
                "litte" => "from_bytes::ByteEndianes::Litte",
                _ => panic!("unsupproted endianes"),
            }
        } else {
            "from_bytes::ByteEndianes::Litte"
        }
        .parse()
        .expect("Could not endianes map method");

        let order: Option<proc_macro2::TokenStream> = if let Some(order) = &args.order {
            Some(
                format!("{:?}", order)
                    .parse()
                    .expect("Could not order method"),
            )
        } else {
            None
        };

        match self {
            Self::FixedArray(kind, count) => {
                let iter = if let Some(register) = &args.register {
                    args.iter.expect("array types need iter option") * 2
                } else {
                    args.iter.expect("array types need iter option") * 2
                };

                let mut method_name = String::from("[");
                let mut args = args.clone();
                args.register = None;
                for i in 0..*count {
                    args.byte = Some(byte + i * iter);
                    method_name += &format!("{},", kind.method(&args));
                }
                method_name += "]";
                //panic!("method_name: {}", method_name);

                let method_name: proc_macro2::TokenStream = method_name
                    .to_string()
                    .parse()
                    .expect("Could not parse map method");

                quote!(#method_name)
            }
            Self::Float32 => {
                let method_name: proc_macro2::TokenStream = "from_bytes::parse_f32"
                    .to_string()
                    .parse()
                    .expect("Could not parse map method");

                let order = order.expect("float32 needs order array");

                quote!(#method_name(data.get(#byte..data.len()).ok_or(format!("out of index: {}", #byte))?, #order)?)
            }
            Self::Into => {
                let method_name: proc_macro2::TokenStream = TRY_INTO_METHOD
                    .to_string()
                    .parse()
                    .expect("Could not parse map method");

                quote!(#method_name(data.get(#byte..data.len()).ok_or(format!("out of index: {}", #byte))?)?)
            }
            Self::UnsignedInteger32 => {
                let method_name: proc_macro2::TokenStream = "from_bytes::parse_u32"
                    .to_string()
                    .parse()
                    .expect("Could not parse map method");

                let order = order.expect("u32 needs order array");

                quote!(#method_name(data.get(#byte..data.len()).ok_or(format!("out of index: {}", #byte))?, #mask, #shift, #order)?)
            }
            _ => {
                let method_name: proc_macro2::TokenStream =
                    format!("from_bytes::parse_{}", self.type_info())
                        .parse()
                        .expect("Could not parse map method");

                quote!(#method_name(data.get(#byte..data.len()).ok_or(format!("out of index: {}", #byte))?, #mask, #shift, #endianes)?)
            }
        }
    }

    fn type_info(&self) -> &'static str {
        match self {
            Self::SignedInteger16 => "i16",
            Self::UnsignedInteger16 => "u16",
            Self::Bool => "bool",
            _ => panic!("called witch wrong type"),
        }
    }
}

#[derive(Debug, Default, Clone)]
struct FieldArgs {
    pub kind: FieldKind,
    pub skip: bool,
    pub byte: Option<usize>,
    pub register: Option<usize>,
    pub default: Option<String>,
    pub order: Option<Vec<u8>>,
    pub mask: Option<u16>,
    pub shift: Option<u16>,
    pub iter: Option<usize>,
    pub endianes: Option<String>,
}

fn extract_meta_str(expr: &MetaValue) -> String {
    match expr {
        MetaValue::Lit(value) => match &value {
            Lit::Str(value_str) => value_str.value(),
            _ => panic!("Only strings supported: {}", value.to_token_stream()),
        },
        _ => panic!("Expected literal key name: {}", expr.into_token_stream()),
    }
}

fn extract_meta_array(expr: &MetaValue) -> Vec<u8> {
    match expr {
        MetaValue::Lit(value) => match &value {
            Lit::Str(value_str) => value_str
                .value()
                .trim_matches('[')
                .trim_matches(']')
                .split(",")
                .into_iter()
                .map(|s| {
                    s.trim()
                        .parse()
                        .expect(&format!("error parsing meta array: {}", s))
                })
                .collect(),
            _ => panic!("Only strings supported: {}", value.to_token_stream()),
        },
        _ => panic!("Expected literal key name: {}", expr.into_token_stream()),
    }
}

fn extract_meta_integer(expr: &MetaValue) -> i32 {
    match expr {
        MetaValue::Lit(value) => match &value {
            Lit::Int(value_integer) => value_integer.base10_parse().expect("integer parse failed"),
            _ => panic!("Only integers supported: {}", value.to_token_stream()),
        },
        _ => panic!("Expected literal key name: {}", expr.into_token_stream()),
    }
}

fn extract_meta_bool(expr: &MetaValue) -> bool {
    match expr {
        MetaValue::Lit(value) => match &value {
            Lit::Bool(value_bool) => value_bool.value(),
            _ => panic!("Only bools supported: {}", value.to_token_stream()),
        },
        _ => panic!("Expected literl key name: {}", expr.to_token_stream()),
    }
}

#[proc_macro_derive(FromBytes, attributes(from_bytes))]
pub fn derive(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input);
    let DeriveInput {
        ident, attrs, data, ..
    } = &input;

    let obj = match data {
        syn::Data::Struct(obj) => obj,
        _ => panic!("Only structs supported in From macro"),
    };

    //determine the field-specific properties
    let field_objs = obj
        .fields
        .iter()
        .map(|field| {
            let field_name = field
                .ident
                .as_ref()
                .expect("Structs must contain named fields")
                .clone();
            let mut props = FieldArgs {
                kind: match &field.ty {
                    syn::Type::Path(path) => {
                        let primitve = path.path.segments.first().unwrap().ident.to_string();
                        match primitve.as_str() {
                            "i16" => FieldKind::SignedInteger16,
                            "u16" => FieldKind::UnsignedInteger16,
                            "u32" => FieldKind::UnsignedInteger32,
                            "f32" => FieldKind::Float32,
                            "bool" => FieldKind::Bool,
                            _ => FieldKind::Into,
                        }
                    }
                    syn::Type::Array(array) => {
                        if let syn::Expr::Lit(exprtlit) = &array.len {
                            let kind = if let syn::Type::Path(path) = &*array.elem {
                                let primitve =
                                    path.path.segments.first().unwrap().ident.to_string();
                                match primitve.as_str() {
                                    "i16" => FieldKind::SignedInteger16,
                                    "u16" => FieldKind::UnsignedInteger16,
                                    "u32" => FieldKind::UnsignedInteger32,
                                    "f32" => FieldKind::Float32,
                                    "bool" => FieldKind::Bool,
                                    _ => FieldKind::Into,
                                }
                            } else {
                                FieldKind::Into
                            };
                            if let syn::Lit::Int(integer) = &exprtlit.lit {
                                FieldKind::FixedArray(
                                    Box::new(kind),
                                    integer.base10_parse().expect("failed persing array len"),
                                )
                            } else {
                                panic!("unsupproted array len type");
                            }
                        } else {
                            panic!("unsupproted array len type");
                        }
                    }
                    _ => panic!("unsupproted type: {:?} found", field.ty),
                },
                ..Default::default()
            };

            field
                .attrs
                .iter()
                .filter(|a| a.path.is_ident("from_bytes"))
                .flat_map(|attr| {
                    attr.parse_args_with(<Punctuated<Meta, Token![,]>>::parse_terminated)
                        .expect("Could not parse 'from' attribute")
                })
                .for_each(|meta| match meta {
                    Meta::Path(path) => match path.to_token_stream().to_string().as_str() {
                        "skip" => {
                            props.skip = true;
                        }
                        _ => panic!("Unrecognised bool property"),
                    },
                    Meta::NameValue(pair) => {
                        match pair.path.to_token_stream().to_string().as_str() {
                            "default" => props.default = Some(extract_meta_str(&pair.value)),
                            "skip" => props.skip = extract_meta_bool(&pair.value),
                            "byte" => props.byte = Some(extract_meta_integer(&pair.value) as usize),
                            "register" => {
                                props.register = Some(extract_meta_integer(&pair.value) as usize)
                            }
                            "order" => props.order = Some(extract_meta_array(&pair.value)),
                            "mask" => props.mask = Some(extract_meta_integer(&pair.value) as u16),
                            "iter" => props.iter = Some(extract_meta_integer(&pair.value) as usize),
                            "shift" => props.shift = Some(extract_meta_integer(&pair.value) as u16),
                            "endianes" => props.endianes = Some(extract_meta_str(&pair.value)),
                            _ => panic!("Unrecognised key value pair"),
                        }
                    }
                    _ => panic!("Expected name value pair"),
                });

            (field_name, props)
        })
        .collect::<Vec<_>>();

    let mut output = proc_macro2::TokenStream::new();

    let fields = field_objs
        .iter()
        .map(|(field_name, field_obj)| {
            let skip = field_obj.skip;
            let default = field_obj.default.clone();
            //panic!("target_value: {:?}", field_obj);
            let target_value = {
                if skip {
                    let method_name: proc_macro2::TokenStream = default
                        .unwrap_or_else(|| SKIP_METHOD.to_string())
                        .parse()
                        .expect("Could not parse skip method");

                    quote!(#method_name)
                } else {
                    field_obj.kind.method(field_obj)
                }
            };
            quote!(#field_name: #target_value)
        })
        .collect::<Vec<_>>();

    //panic!("fields: {:?}", fields);

    let type_impl = quote! {
        impl ::std::convert::TryFrom<&[u8]> for #ident {
            type Error = Box<dyn std::error::Error + Send + Sync>;

            fn try_from(data: &[u8]) -> Result<Self, Self::Error> {
                Ok(Self {
                    #(#fields),*
                })
            }
        }
        impl ::std::convert::TryFrom<&[u16]> for #ident {
            type Error = Box<dyn std::error::Error + Send + Sync>;

            fn try_from(data: &[u16]) -> Result<Self, Self::Error> {
                let data_u8 = from_bytes::zerocopy::AsBytes::as_bytes(data);
                data_u8.try_into()
            }
        }
    };
    output.extend(type_impl);

    output.into()
}
