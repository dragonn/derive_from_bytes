use from_bytes::FromBytes;

#[derive(Debug, FromBytes)]
pub struct DataRaw {
    #[from_bytes(register = 0, iter = 27)]
    pub transducers: [Transducer; 2],
}

#[derive(Debug, FromBytes)]
pub struct Transducer {
    #[from_bytes(register = 0, iter = 3)]
    pub detectors: [Detector; 6],
}

#[derive(Debug, FromBytes)]
pub struct Detector {
    #[from_bytes(register = 0, order = "[1, 0, 3, 2]")]
    measuring_result: f32,

    #[from_bytes(register = 1, mask = 0x00FF)]
    alert_alarm: bool,
    #[from_bytes(register = 1, mask = 0xFF00)]
    danger_alarm: bool,
}

fn main() {
    println!("Hello, world!");
    let data: Vec<u8> = vec![];
    let vibro: Band = data.as_slice().try_into().unwrap();
}
