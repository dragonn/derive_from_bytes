#![feature(prelude_import)]
#[prelude_import]
use std::prelude::rust_2021::*;
#[macro_use]
extern crate std;
use from_bytes::FromBytes;
pub struct VibroVC1800 {
    transducers: [Transducer; 4],
}
#[automatically_derived]
#[allow(unused_qualifications)]
impl ::core::fmt::Debug for VibroVC1800 {
    fn fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
        match *self {
            VibroVC1800 {
                transducers: ref __self_0_0,
            } => {
                let debug_trait_builder =
                    &mut ::core::fmt::Formatter::debug_struct(f, "VibroVC1800");
                let _ = ::core::fmt::DebugStruct::field(
                    debug_trait_builder,
                    "transducers",
                    &&(*__self_0_0),
                );
                ::core::fmt::DebugStruct::finish(debug_trait_builder)
            }
        }
    }
}
pub struct Transducer {
    #[from_bytes(byte = 1, iter = 10)]
    bands: [Band; 4],
    #[from_bytes(byte = 2)]
    band: Band,
    #[from_bytes(byte = 1, iter = 10, order = "LL_LH_HL_HH")]
    floats: [f32; 4],
}
#[automatically_derived]
#[allow(unused_qualifications)]
impl ::core::fmt::Debug for Transducer {
    fn fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
        match *self {
            Transducer {
                bands: ref __self_0_0,
                band: ref __self_0_1,
                floats: ref __self_0_2,
            } => {
                let debug_trait_builder =
                    &mut ::core::fmt::Formatter::debug_struct(f, "Transducer");
                let _ =
                    ::core::fmt::DebugStruct::field(debug_trait_builder, "bands", &&(*__self_0_0));
                let _ =
                    ::core::fmt::DebugStruct::field(debug_trait_builder, "band", &&(*__self_0_1));
                let _ =
                    ::core::fmt::DebugStruct::field(debug_trait_builder, "floats", &&(*__self_0_2));
                ::core::fmt::DebugStruct::finish(debug_trait_builder)
            }
        }
    }
}
impl ::std::convert::TryFrom<&[u8]> for Transducer {
    type Error = Box<dyn std::error::Error>;
    fn try_from(data: &[u8]) -> Result<Self, Self::Error> {
        Ok(Self {
            bands: [
                core::convert::TryInto::try_into(
                    data.get(1usize..data.len()).ok_or("out of index")?,
                )?,
                core::convert::TryInto::try_into(
                    data.get(11usize..data.len()).ok_or("out of index")?,
                )?,
                core::convert::TryInto::try_into(
                    data.get(21usize..data.len()).ok_or("out of index")?,
                )?,
                core::convert::TryInto::try_into(
                    data.get(31usize..data.len()).ok_or("out of index")?,
                )?,
            ],
            band: core::convert::TryInto::try_into(
                data.get(2usize..data.len()).ok_or("out of index")?,
            )?,
            floats: [
                from_bytes::parse_f32(
                    data.get(1usize..data.len()).ok_or("out of index")?,
                    from_bytes::ByteOrderf32::LL_LH_HL_HH,
                )?,
                from_bytes::parse_f32(
                    data.get(11usize..data.len()).ok_or("out of index")?,
                    from_bytes::ByteOrderf32::LL_LH_HL_HH,
                )?,
                from_bytes::parse_f32(
                    data.get(21usize..data.len()).ok_or("out of index")?,
                    from_bytes::ByteOrderf32::LL_LH_HL_HH,
                )?,
                from_bytes::parse_f32(
                    data.get(31usize..data.len()).ok_or("out of index")?,
                    from_bytes::ByteOrderf32::LL_LH_HL_HH,
                )?,
            ],
        })
    }
}
pub struct Band {
    #[from_bytes(byte = 0, order = "LL_LH_HL_HH")]
    measuring_result: f32,
    #[from_bytes(byte = 1, mask = 0x0F)]
    alert_alarm: bool,
    #[from_bytes(byte = 2)]
    danger_alarm: bool,
}
#[automatically_derived]
#[allow(unused_qualifications)]
impl ::core::fmt::Debug for Band {
    fn fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
        match *self {
            Band {
                measuring_result: ref __self_0_0,
                alert_alarm: ref __self_0_1,
                danger_alarm: ref __self_0_2,
            } => {
                let debug_trait_builder = &mut ::core::fmt::Formatter::debug_struct(f, "Band");
                let _ = ::core::fmt::DebugStruct::field(
                    debug_trait_builder,
                    "measuring_result",
                    &&(*__self_0_0),
                );
                let _ = ::core::fmt::DebugStruct::field(
                    debug_trait_builder,
                    "alert_alarm",
                    &&(*__self_0_1),
                );
                let _ = ::core::fmt::DebugStruct::field(
                    debug_trait_builder,
                    "danger_alarm",
                    &&(*__self_0_2),
                );
                ::core::fmt::DebugStruct::finish(debug_trait_builder)
            }
        }
    }
}
impl ::std::convert::TryFrom<&[u8]> for Band {
    type Error = Box<dyn std::error::Error>;
    fn try_from(data: &[u8]) -> Result<Self, Self::Error> {
        Ok(Self {
            measuring_result: from_bytes::parse_f32(
                data.get(0usize..data.len()).ok_or("out of index")?,
                from_bytes::ByteOrderf32::LL_LH_HL_HH,
            )?,
            alert_alarm: from_bytes::parse_bool(
                data.get(1usize..data.len()).ok_or("out of index")?,
                Some(15),
                None,
                from_bytes::ByteEndianes::Big,
            )?,
            danger_alarm: from_bytes::parse_bool(
                data.get(2usize..data.len()).ok_or("out of index")?,
                None,
                None,
                from_bytes::ByteEndianes::Big,
            )?,
        })
    }
}
fn main() {
    ::std::io::_print(::core::fmt::Arguments::new_v1(&["Hello, world!\n"], &[]));
    let data: Vec<u8> = ::alloc::vec::Vec::new();
    let vibro: Band = data.as_slice().try_into().unwrap();
}
